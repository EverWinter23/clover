#!/bin/bash

mkdir voc
cd voc

# download PASCAL_VOC 2007 dataset-- training + validation set
wget -c https://pjreddie.com/media/files/VOCtrainval_06-Nov-2007.tar

# download PASCAL_VOC 2007 testset
wget -c https://pjreddie.com/media/files/VOCtest_06-Nov-2007.tar

# extract dataset
tar xf VOCtrainval_06-Nov-2007.tar
tar xf VOCtest_06-Nov-2007.tar

wget -c https://pjreddie.com/media/files/voc_label.py

python voc_label.py
